const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const _ = require("underscore");
const common = require("../common");

const authenticate = async ({email, password}) => {
    const users = common.readJsonFile();
    const user = _.find(users, function (user) {
    return user.email == email;
  });
  if (!user) return { error: "Email is invalid" };
  const isPasswordTrue = await bcrypt.compare(password, user.password);
  if (isPasswordTrue) {
    var token = jwt.sign(
      {
        id: user.userId,
      },
      process.env.JWT_SECRET,
      {}
    );
    user.token = token;
    common.writeJsonFile(users);
    return { token: token };
  } else {
    return { error: "Password is invalid" };
  }
};

const mapUser = ({ email, userId, name }) => {
  return { email, userId, name };
};

module.exports = {
  login: async (req, res) => {
    const body = req.body;
    if (!body.email ) return { message: "Email Required" };
    if (!body.password) return { message: "Password Required" };
    const response = await authenticate(body);
    if (response.error) {
      return {
        message: response.error,
        error: "BadRequestError",
      };
    } else {
      return {
        data: {
          token: response.token,
        },
      };
    }
  },

  find: async (req, res) => {
    const users = common.readJsonFile();
    const user = _.find(users, function (user) {
      return user.userId == req.user.id;
    });
    if (!user) {
      return {
        message: "User Not Found",
        error: "BadRequestError",
      };
    } else {
      return {
        data: {
          user: mapUser(user),
        },
      };
    }
  },
};
