const router = require("express").Router();
const asyncMiddleware = require("../lib/asyncMiddleware");
const { usersController } = require("../controllers");

// Login, Lgout
router.post("/login", asyncMiddleware(usersController.login));
router.get("/user", asyncMiddleware(usersController.find));

module.exports = router;
