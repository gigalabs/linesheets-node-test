const fs = require("fs");

let usersJson = {
  users: [],
};

module.exports = {
  readJsonFile: () => {
    let usersData = fs.readFileSync("users.json");
    return JSON.parse(usersData).users;
  },
  
  writeJsonFile: (users) => {
    usersJson.users = users;
    var json = JSON.stringify(usersJson);
    fs.writeFile("users.json", json, "utf8", (res) => {
    });
  }
}