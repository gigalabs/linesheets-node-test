const { OK, ERROR, UNAUTHORIZED } = require('../lib/constants').STATUS;
const respond = {
  do: function(res, status, message="Success", data={}) {

    let code = OK;
    if (status == 401) {
      code = UNAUTHORIZED;
    } else if (status == 400 || status == 500 || status == 404) {
      code = ERROR;
    }

    return res.status(status).send({
      code: code,
      message: message,
      data: data || {},
    })
  },
  withError: function(res, err) {
    switch(err.name) {
      case 'BadRequestError':
        return respond.do(res, err.status || 400, err.errors[0].message);
      case 'ResourceNotFound':
        return respond.do(res, 404, err.errors[0].message);
      case 'UnauthorizedError':
        return respond.do(res, 401, "Invalid token");
      default:
        return respond.do(res, err.status || 500, "Sorry, Some Technical Issue");
    }
  }
}
module.exports = respond;
