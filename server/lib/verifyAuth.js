const fs = require("fs");
const _ = require("underscore");

readJsonFile = () => {
  let usersData = fs.readFileSync("users.json");
  return JSON.parse(usersData).users;
}

module.exports = async (req, payload, done) => { 
  let token = req.headers.authorization.split(" ")[1];
  if (payload) {
    const users = readJsonFile();
    const user = _.find(users, function (user) {
      return user.token == token
    });
    if (!user) return done(null, true);
  }
  return done(null, false);
}