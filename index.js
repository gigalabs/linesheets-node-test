const express = require("express");
const bodyParser = require('body-parser');
const jwt = require("express-jwt");
const verifyAuth = require('./server/lib/verifyAuth');
const respondWithError = require('./server/utils/respond').withError;
const http = require('http');
require('dotenv').config()

const app = express();
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept, Authorization"
  );
  next();
});

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));

app.use(
  jwt({
    secret: process.env.JWT_SECRET,
    isRevoked: verifyAuth,
  }).unless({
    path: ["/api/login"],
  })
);

app.use("/api", require("./server/routes"));

app.use(function (err, req, res, next) {
  return respondWithError(res, err);
});



const port = parseInt(process.env.PORT, 10) || 8000;
app.set('port', port);
const server = http.createServer(app);
server.listen(port, function(){
	console.log('Server is running on port: '+port);
});